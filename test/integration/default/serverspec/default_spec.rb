require 'serverspec'

set :backend, :exec

describe command('phantomjs --version') do
  its(:stdout) { should match '2.1.1' }
end
