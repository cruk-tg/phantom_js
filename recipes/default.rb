#
# Cookbook Name:: phantom_js
# Recipe:: default
#
# Copyright 2016, University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
version = 'phantomjs-2.1.1-linux-x86_64'
src = "https://bitbucket.org/ariya/phantomjs/downloads/#{version}.tar.bz2"
#src = "https://uoe-my.sharepoint.com/personal/pmitche2_ed_ac_uk/_layouts/15/guestaccess.aspx?guestaccesstoken=VMnZojayRJuqfAx%2fY3n99qcEBN%2ftNQIDSE0NeX9vHEw%3d&docid=0ed7be54e629a44e88168d7c2e08bb143"
sha256 = '86dd9a4bf4aee45f1a84c9f61cf1947c1d6dce9b9e8d2a907105da7852460d2f'
dist = "#{Chef::Config['file_cache_path']}/#{version}.tar.bz2"

package 'libfontconfig1'

execute 'create link' do
  command "ln -s /usr/local/#{version}/bin/phantomjs /usr/local/bin"
  action :nothing
end

execute 'extract_module' do
  cwd ::File.dirname(dist)
  command "tar jxf #{dist} -C /usr/local"
  notifies :run, 'execute[create link]'
  action :nothing
end

remote_file dist do
  source src
  checksum sha256
  owner 'root'
  group 'root'
  not_if do FileTest.executable?("/usr/local/#{version}/bin/phantomjs") end
  notifies :run, 'execute[extract_module]'
end
