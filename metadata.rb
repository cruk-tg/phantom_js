name             'phantom_js'
maintainer       'University of Edinburgh'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license          'Apache 2.0'
description      'Installs/Configures phantom_js'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.5'

supports 'ubuntu'
