phantom_js CHANGELOG
===================

This file is used to list changes made in each version of the phantom_js cookbook.

1.0.5
-----
- Paul Mitchell

Change to offical website

1.0.4
-----
- Paul Mitchell - Initial release of phantom_js

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
